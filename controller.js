const C = {
    IS_MOVING_LEFT: null,
    IS_MOVING_RIGHT: null,
    GAMEMOD: "MENU",
    init(){
        M.init();
        V.init();
        C.gameLoop();
        C.IS_MOVING_RIGHT = M.data.paddle.isMovingRight;
        C.IS_MOVING_RIGHT = M.data.paddle.isMovingLeft;
    },

    getScore(){
        return M.SCORE
    },

    sync_model_and_view(){
        if(C.IS_MOVING_LEFT){
            C.moveSpaceShipLeft();
        }
        if(C.IS_MOVING_RIGHT){
            C.moveSpaceShipRight();
        }
        return [M.data, M.LEVEL, M.MAP];
    },

    gameLoop: function () {
        switch (C.GAMEMOD) {
            case "MENU":
                V.menu();
                break;

            case "PLAYING":
                const data = C.sync_model_and_view();
                V.render(data);
                if(M.breakableBricks === 0 && M.LEVEL !== 6){
                    C.nextLevel();
                }
                break;

            case "LOSE":
                console.log("LOSE");
                break;

            case "END":
                console.log("END");
                break;

        }
        window.requestAnimationFrame(C.gameLoop);
    },

    previousLevel(){
        if(M.LEVEL > 1){
            C.resetParams();
            M.LEVEL = M.LEVEL - 1;
            V.clearCanvas();
        }
        M.init();
    },

    nextLevel(){
        M.LEVEL = M.LEVEL + 1;
        V.clearCanvas();

        if(M.LEVEL === 7){
            C.GAMEMOD = 'END'
        } else {
            C.resetParams();
            M.init();
        }

    },

    moveSpaceShipRight() {
        let newPosX = M.data.paddle.posX + (M.controls * 10);

        if(newPosX < (V.canvas.width - M.data.paddle.width)){
            if(!M.data.ball.isMoving){
                M.data.ball.posX = M.data.ball.posX + (M.controls * 10);
            }
            M.data.paddle.moveRight();
        }
    },

    moveSpaceShipLeft() {
        let newPosX = M.data.paddle.posX - (M.controls * 10);

        if(newPosX >0){
            if(!M.data.ball.isMoving){
                M.data.ball.posX = M.data.ball.posX - (M.controls * 10);
            }
            M.data.paddle.moveLeft();
        }
    },

    launchBall(){
        if(!M.data.ball.isMoving){
            C.GAMEMOD = "PLAYING";
            M.data.ball.isMoving = true;
        }
    },

    moveBall(){
        if( M.data.ball.posY - M.data.ball.height < M.data.ball.speed ){
            M.MOVE_Y = -M.MOVE_Y;
            C.playSound('hit');
        }

        if( M.data.ball.posX + M.data.ball.width > V.canvas.width ){
            M.MOVE_X = -M.MOVE_X;
            C.playSound('hit');
        }

        M.data.ball.moveBall(M.MOVE_X, M.MOVE_Y);
    },

    checkCollision() {
        M.collision()
    },

    playSound(url){
        V.playSound(M.data.sounds, url);
    },

    resetParams(){
        M.data.ball= null;
        M.data.paddle= null;
        M.data.sounds= [];
        M.data.bonus= [];
        M.data.blocks= [];
        M.MAP = [];
        M.breakableBricks = 0;
    }
};
