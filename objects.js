class BRIQUE {
    constructor(posX,posY, width, height, lifes, color, imgX, imgY) {
        this._posX = posX;
        this._posY = posY;
        this._width = width;
        this._height = height;
        this._color = color;
        this._number_of_lifes = lifes;
        this._imgX = imgX;
        this._imgY = imgY;

    }

    get posX() {
        return this._posX;
    }
    get posY() {
        return this._posY;
    }
    get width() {
        return this._width;
    }
    get height() {
        return this._height;
    }
    get color() {
        return this._color;
    }
    get lifes() {
        return this._number_of_lifes;
    }
    get imgX() {
        return this._imgX;
    }
    get imgY() {
        return this._imgY;
    }
    set imgX(value) {
        this._imgX = value;
    }
    set imgY(value) {
        this._imgY = value;
    }
    set lifes(value) {
        this._number_of_lifes = value;
    }
    set posX(value) {
        this._posX = value;
    }
    set posY(value) {
        this._posY = value;
    }
    set width(value) {
        this._width = value;
    }
    set height(value) {
        this._height = value;
    }
    set color(value) {
        this._color = value;
    }
}

class BRIQUE_LV_2 extends BRIQUE {
    constructor(posX, posY, width, height, lifes, color, imgX, imgY){ super(posX, posY, width, height, lifes, "blue", imgX, imgY) }
}

class BRIQUE_LV_3 extends BRIQUE {
    constructor(posX, posY, width, height, lifes, color, imgX, imgY){ super(posX, posY, width, height, lifes, "red", imgX, imgY) }
}

class SUPER_BRIQUE extends BRIQUE {
    constructor(posX, posY, width, height, lifes, color, imgX, imgY){ super(posX, posY, width, height, lifes, "red", imgX, imgY) }
}

class BONUS {
    constructor(posX,posY, color, type) {
        this._posX = posX;
        this._posY = posY;
        this._width = 80;
        this._height = 40;
        this._color = color;
        this._type = type;
    }

    updatePos(){
        this._posY = this._posY + 2;
    }

    get width() {
        return this._width;
    }
    get height() {
        return this._height;
    }
    get posX() {
        return this._posX;
    }
    get posY() {
        return this._posY;
    }
    get type() {
        return this._type;
    }
    get color() {
        return this._color;
    }
    set posX(value) {
        this._posX = value;
    }
    set posY(value) {
        this._posY = value;
    }
    set color(value) {
        this._color = value;
    }
    set type(value) {
        this._type = value;
    }
}


class PADDLE {
    constructor(posX, posY, width, color) {
        this._posX = posX;
        this._posY = posY;
        this._width = width;
        this._height = 10;
        this._color = color;
        this._isMovingLeft = false;
        this._isMovingRight = false;
    }

    moveRight(){
        this._posX =  this._posX + (M.controls * 10);
    }

    moveLeft(){
        this._posX = this._posX - (M.controls * 10);
    }

    get posX() {
        return this._posX;
    }
    get posY() {
        return this._posY;
    }
    get width() {
        return this._width;
    }
    get height() {
        return this._height;
    }
    get color() {
        return this._color;
    }
    get isMovingLeft() {
        return this._isMovingLeft;
    }
    get isMovingRight() {
        return this._isMovingRight;
    }
    set posX(value) {
        this._posX = value;
    }
    set posY(value) {
        this._posY = value;
    }
    set width(value) {
        this._width = value;
    }
    set color(value) {
        this._color = value;
    }
    set isMovingLeft(value) {
        this._isMovingLeft = value;
    }
    set isMovingRight(value) {
        this._isMovingRight = value;
    }
}

class BALL {
    constructor(posX, posY, color) {
        this._posX = posX;
        this._posY = posY;
        this._width = 10;
        this._height = 10;
        this._color = color;
        this._radius = 10;
        this._lifes = 3;
        this._speed = 4;
        this._isMoving = false;
    }

    moveBall(dx, dy){
        this.posX += dx;
        this.posY += dy;
    }

    get posX() {
        return this._posX;
    }
    get posY() {
        return this._posY;
    }
    get width() {
        return this._width;
    }
    get height() {
        return this._height;
    }
    get color() {
        return this._color;
    }
    get speed() {
        return this._speed;
    }
    get isMoving() {
        return this._isMoving;
    }
    get radius() {
        return this._radius;
    }
    get lifes() {
        return this._lifes;
    }
    set posX(value) {
        this._posX = value;
    }
    set posY(value) {
        this._posY = value;
    }
    set isMoving(value) {
        this._isMoving = value;
    }
    set speed(value){
        this._speed = value;
    }
    set lifes(value) {
        this._lifes = value;
    }
}

const LEVEL_1 = [
    [3, 0, 0, 0, 0, 0],
    [3, 0, 1, 1, 0, 0],
    [3, 1, 2, 2, 1, 0],
    [3, 0, 0, 0, 0, 0],
    [3, 0, 0, 0, 0, 0],
    [3, 0, 0, 0, 0, 0],
    [3, 0, 0, 0, 0, 0],
    [3, 0, 0, 0, 0, 0],
    [3, 1, 2, 2, 1, 0],
    [3, 0, 1, 1, 0, 0],
    [3, 0, 0, 0, 0, 0]
];

const LEVEL_2 = [
    [3, 3, 3, 3, 3, 3],
    [3, 0, 1, 1, 0, 0],
    [3, 1, 3, 3, 1, 0],
    [3, 0, 0, 0, 0, 0],
    [3, 0, 0, 2, 0, 0],
    [3, 0, 2, 2, 2, 0],
    [3, 0, 0, 2, 0, 0],
    [3, 0, 0, 0, 0, 0],
    [3, 1, 3, 3, 1, 0],
    [3, 0, 1, 1, 0, 0],
    [3, 3, 3, 3, 3, 3]
];

const LEVEL_3 = [
    [3, 0, 3, 0, 3, 0],
    [3, 0, 1, 1, 0, 0],
    [3, 1, 3, 3, 1, 0],
    [1, 0, 0, 2, 0, 0],
    [1, 0, 0, 2, 0, 2],
    [3, 0, 2, 3, 2, 0],
    [1, 0, 0, 2, 0, 2],
    [1, 0, 0, 2, 0, 0],
    [3, 1, 3, 3, 1, 0],
    [3, 0, 1, 1, 0, 0],
    [3, 0, 3, 0, 3, 0]
];

const LEVEL_4 = [
    [4, 2, 1, 1, 1, 2],
    [4, 4, 2, 2, 2, 4],
    [4, 4, 4, 2, 4, 4],
    [4, 4, 4, 4, 4, 4],
    [4, 4, 4, 4, 4, 4],
    [4, 4, 4, 4, 4, 4],
    [4, 4, 4, 4, 4, 4],
    [4, 4, 4, 4, 4, 4],
    [4, 4, 4, 2, 4, 4],
    [4, 4, 2, 1, 2, 4],
    [4, 2, 1, 1, 1, 2]
];

const LEVEL_5 = [
    [4, 4, 4, 4, 4, 4],
    [4, 4, 4, 4, 4, 4],
    [4, 4, 4, 4, 4, 4],
    [4, 4, 4, 4, 4, 4],
    [4, 3, 4, 3, 4, 4],
    [4, 4, 2, 4, 4, 4],
    [4, 3, 4, 3, 4, 4],
    [4, 4, 4, 4, 4, 4],
    [4, 4, 4, 4, 4, 4],
    [4, 4, 4, 4, 4, 4],
    [4, 4, 4, 4, 4, 4]
];

const LEVEL_6 = [
    [4, 3, 2, 2, 2, 3],
    [4, 4, 3, 2, 3, 4],
    [4, 4, 4, 3, 4, 4],
    [4, 4, 4, 4, 4, 4],
    [4, 4, 4, 4, 4, 4],
    [4, 4, 4, 4, 4, 4],
    [4, 4, 4, 4, 4, 4],
    [4, 4, 4, 4, 4, 4],
    [4, 4, 4, 3, 4, 4],
    [4, 4, 3, 2, 3, 4],
    [4, 3, 2, 2, 2, 3]
];
