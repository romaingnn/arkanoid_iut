const V = {
    canvas:null,
    context:null,
    sound : null,
    sprites: null,
    init(){
        V.canvas = document.getElementById("app");
        V.context = V.canvas.getContext("2d");
        V.SCORE_DIV = document.querySelector('#currentScore');
        V.sprites = new Image();
        V.sprites.src = 'assets/sprites.png';
        V.render();
        V.binders()
    },

    clearCanvas(){
        V.context.clearRect(0, 0, V.canvas.width, V.canvas.height);
    },
    render(data) {
        V.canvas.style.backgroundImage = "url('assets/bg.png')";
        V.SCORE_DIV.innerHTML = C.getScore();
        if (data) {
            V.context.clearRect(0, 0, V.canvas.width, V.canvas.height);
            data = data[0];
            const blocks = data.blocks;

            V.context.drawImage(V.sprites, 0, 51, 100, 10, data.paddle.posX, data.paddle.posY, data.paddle.width, data.paddle.height);

            for (let i = 0; i < blocks.length; i++) {
                    const bloc = blocks[i];
                    V.context.drawImage(V.sprites, bloc.imgX, bloc.imgY, bloc.width, bloc.height, bloc.posX, bloc.posY, bloc.width, bloc.height);
            }

            for (let i = 0; i <data.bonus.length ; i++) {
                const bonus =  data.bonus[i];
                V.context.fillStyle = bonus.color;
                V.context.beginPath();
                V.context.rect(bonus.posX, bonus.posY, bonus.width, bonus.height);
                V.context.closePath();
                V.context.fill();
                bonus.updatePos();
            }

            V.context.fillStyle = data.ball.color;
            V.context.beginPath();
            V.context.arc(data.ball.posX, data.ball.posY, data.ball.radius, 0, 2 * Math.PI);
            V.context.closePath();
            V.context.fill();


            const heart_full = new Image();
            heart_full.src = "assets/heart_full.png";

            const heart_empty = new Image();
            heart_empty.src = "assets/heart_empty.png";
            for (let i = 0; i < data.ball.lifes; i++) {
                V.context.drawImage(heart_full, (V.canvas.width + 40 * i) - 150, 10, 30, 30);
            }

            for (let i = 0; i <  (3 - data.ball.lifes); i++) {
                V.context.drawImage(heart_empty, (V.canvas.width - 40 * i) - 70, 10, 30, 30);
            }

            if (data.ball.isMoving) {
                C.moveBall();
                C.checkCollision()
            }
        }
    },

    menu(){
        V.sound = document.getElementById('music');
        V.canvas.style.background = "#000000";
        function getMousePos(canvas, event) {
            const rect = canvas.getBoundingClientRect();
            return {
                x: event.clientX - rect.left,
                y: event.clientY - rect.top
            };
        }
        function isInside(pos, rect){
            return pos.x > rect.x && pos.x < rect.x+rect.width && pos.y < rect.y+rect.heigth && pos.y > rect.y
        }
        const button = {
            x:412,
            y:350,
            width:200,
            heigth:100
        };
        V.context.clearRect(0, 0, V.canvas.width, V.canvas.height);
        const image = document.getElementById("source");
        V.context.drawImage(image, 213, 50, 600, 187);
        V.context.fillStyle = '#ffffff';
        V.context.font = '40px Minecraft';
        V.context.fillText('START', 447, 415);
        V.context.font = '15px Minecraft';
        V.context.fillText('© 1986 TATO CORP JAPAN', 425, V.canvas.height - 30);
        V.context.fillText('ALL RIGHTS RESERVED', 435, V.canvas.height - 15);
        V.canvas.addEventListener('click', function(evt) {
            var mousePos = getMousePos(V.canvas, evt);
            if (isInside(mousePos,button)) {
                V.sound.pause();
                C.GAMEMOD = "PLAYING";
            }
        }, false);
        },

    binders(){
        document.addEventListener('keydown', function(e){
            if(e.key === "ArrowRight"){
                C.IS_MOVING_RIGHT = true;
            }

            if(e.key === "ArrowLeft"){
                C.IS_MOVING_LEFT = true;
            }

            if(e.key === " " && C.GAMEMOD === "PLAYING"){
                C.launchBall();
            }
        });

        document.addEventListener('keyup', function(e){
            if(e.key === "ArrowRight"){
                C.IS_MOVING_RIGHT = false;
            }

            if(e.key === "ArrowLeft"){
                C.IS_MOVING_LEFT = false;
            }

        });
    },


    playSound(array, url){
        const audio = new Audio('assets/sounds/' + url + '.wav');
        audio.addEventListener('ended', function(){
            array.splice(array.indexOf(audio), 1);
        });
        audio.play();
        array.push(audio);
    }
};
