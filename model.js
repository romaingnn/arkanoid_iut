const tools = {
    hitbox: (r1, r2) => {
        let collision, dw, dh,
            overlapX, overlapY, vx, vy;

        vx = (r1.posX + r1.width / 2) - (r2.posX + r2.width / 2);
        vy = (r1.posY + r1.height / 2) - (r2.posY + r2.height / 2);

        dw = r1.width / 2 + r2.width / 2;
        dh = r1.height / 2 + r2.height / 2;

        if (Math.abs(vx) < dw) {
            if (Math.abs(vy) < dh) {
                overlapX = dw - Math.abs(vx);
                overlapY = dh - Math.abs(vy);
                if (overlapX >= overlapY) {
                    if (vy > 0) {
                        collision = "top";
                        r1.y = r1.y + overlapY;
                    } else {
                        collision = "bottom";
                        r1.y = r1.y - overlapY;
                    }
                } else {
                    if (vx > 0) {
                        collision = "left";
                        r1.x = r1.x + overlapX;
                    } else {
                        collision = "right";
                        r1.x = r1.x - overlapX;
                    }
                }
            } else {
                collision = "no collision";
            }
        } else {
            collision = "no collision";
        }
        return collision
    }
};

const M = {
    data:{
        ball:null,
        paddle: null,
        blocks: [],
        sounds: [],
        bonus: []
    },
    LEVEL: 1,
    MAP: [],
    MOVE_X: 0,
    MOVE_Y: 0,
    tmp: 0,
    SCORE: 0,
    bonusProbability : 0.2,
    breakableBricks: 0,
    bonusIsActive : false,
    controls: 1,
    init(){
        switch (M.LEVEL) {
            case 1:
                M.MAP = LEVEL_1;
                break;
            case 2:
                M.MAP = LEVEL_2;
                break;
            case 3:
                M.MAP = LEVEL_3;
                break;
            case 4:
                M.MAP = LEVEL_4;
                break;
            case 5:
                M.MAP = LEVEL_5;
                break;
            case 6:
                M.MAP = LEVEL_6;
                break;
        }

        M.initMap(M.MAP);
        this.data.paddle = new PADDLE(462, 760, 100, "blue");
        this.data.ball = new BALL(507, 750, "red");
        M.MOVE_X = this.data.ball.speed;
        M.MOVE_Y = -this.data.ball.speed;
    },

    newBall(lifes){
        this.data.ball.posX = 507;
        this.data.ball.posY = 750;
        this.data.paddle.posX = 462;
        this.data.paddle.posY = 760;
        this.data.ball.isMoving = false;
        this.data.ball.lifes = lifes;
    },

    checkCollisionWithBloc(){
        M.data.blocks.forEach((bloc) => {
            const collisionSide = tools.hitbox(M.data.ball, bloc);
            if (collisionSide !== "no collision") {
                switch (bloc.lifes) {
                    case 1:
                        const blocIndex = M.data.blocks.indexOf(bloc);
                        const currentBlock =  M.data.blocks.splice(blocIndex, 1)[0];
                        M.SCORE += 10;
                        const generatedProba =  Math.random() * 10;
                        if(M.bonusIsActive === false){
                            if(generatedProba > 0 && generatedProba <= 4){
                                const generatedBonus = M.getBonus(generatedProba);
                                const bonus = new BONUS(currentBlock.posX, currentBlock.posY + currentBlock.height, "yellow", generatedBonus);
                                M.data.bonus.push(bonus);
                            }
                            if(generatedProba > 4 && generatedProba <= 9){
                                const generatedBonus = M.getMalus(generatedProba);
                                const malus = new BONUS(currentBlock.posX, currentBlock.posY + currentBlock.height, "dark", generatedBonus);
                                M.data.bonus.push(malus);
                            }
                        }
                        M.breakableBricks--;
                        break;

                    case 2:
                        bloc.lifes--;
                        M.SCORE += 30;
                        bloc.imgX = 240;
                        break;

                    case 3:
                        bloc.lifes--;
                        M.SCORE += 30;
                        bloc.imgX = 160;
                        break;

                    case null:
                        break;
                }
                C.playSound('hit');
            }
            if (collisionSide !== "no collision" && collisionSide === "left" || collisionSide ==="right")
                M.MOVE_X = -M.MOVE_X;
            else if (collisionSide !== "no collision" && collisionSide === "top" || collisionSide ==="bottom")
                M.MOVE_Y = -M.MOVE_Y;
        });

        M.data.bonus.forEach((bonus) => {
            const collisionSide = tools.hitbox(M.data.paddle, bonus);
            if (collisionSide === "top") {
                switch (bonus.type) {
                    case "malus_bar":
                        M.malus_SizeBar();
                        break;
                    case "bonus_bar":
                        M.bonus_SizeBar();
                        break;
                    case "bonus_life":
                        M.bonus_addLifes();
                        break;
                    case "malus_reverse_controls":
                        M.malus_reverse_controls();
                        break;
                }
                const bonusIndex = M.data.bonus.indexOf(bonus);
                M.data.bonus.splice(bonusIndex, 1);
            }
        })
    },

    collision(){
        const ball = M.data.ball;
        const paddle = M.data.paddle;
        M.checkCollisionWithBloc();
        if(ball.posY > (paddle.posY + paddle.height + ball.height))
        {
            if(ball.lifes === 0){
                C.playSound('lose');
                C.GAMEMOD = "LOSE";
            } else {
                const lifes = ball.lifes - 1;
                M.newBall(lifes);
            }
        } else {
        }
        if(ball.posY + ball.height === paddle.posY
            && ball.posX > paddle.posX
            && ball.posX < (paddle.posX + paddle.width)
        )
        {
            // M.MOVE_X = Math.min(Math.max((((ball.posX+ball.radius)-paddle.coordX) / paddle.width)*2 - 1, -0.6), 0.6);
            // M.MOVE_Y = -(1-Math.abs(M.MOVE_X));
            M.MOVE_Y = -M.MOVE_Y;
            C.playSound('paddle');
        }
        if(ball.posX < 0 )
        {
            M.MOVE_X = -M.MOVE_X;
        }
    },

    initMap(map){
        for (let i = 0; i <map.length ; i++) {
            for (let j = 0; j <map[i].length ; j++) {
                switch (map[i][j]) {
                    case 0:
                        M.data.blocks.push(new BRIQUE(50+i*85, 50+j*45,80,40,1, null, 240, 0));
                        M.breakableBricks++;
                        break;

                    case 1:
                        M.data.blocks.push(new BRIQUE_LV_2(50+i*85, 50+j*45,80,40,2, null,160, 0));
                        M.breakableBricks++;
                        break;

                    case 2:
                        M.data.blocks.push(new BRIQUE_LV_3(50+i*85, 50+j*45,80,40,3, null,480, 0));
                        M.breakableBricks++;
                        break;

                    case 3:
                        M.data.blocks.push(new SUPER_BRIQUE(50+i*85, 50+j*45,80,40,null, null,560, 0));
                        break;
                }
            }
        }
    },

    getBonus(bonus){
        let bonusGenerated = null;
        if (bonus > 0 && bonus <= 1) {
            M.bonusIsActive = true;
            bonusGenerated = "bonus_bar";

        } else if (bonus > 1 && bonus <= 2){
            bonusGenerated = "bonus_life";
        } else if (bonus > 2 && bonus <= 3){

        } else if (bonus > 3 && bonus <= 4){

        }
        return bonusGenerated;
    },

    getMalus(bonus){
        let bonusGenerated = null;
        if (bonus > 4 && bonus <= 6) {
            M.bonusIsActive = true;
            bonusGenerated = "malus_bar";
        } else if (bonus > 6 && bonus <= 7){
            M.bonusIsActive = true;
            bonusGenerated = "malus_reverse_controls";
        } else if (bonus > 7 && bonus <= 8){

        } else if (bonus > 8 && bonus <= 9){

        }
        return bonusGenerated;
    },

    resetBonus(){
        M.bonusIsActive = false;
        console.log('Bonus Is Active')
    },

    bonus_SizeBar(){
        M.data.paddle.width =  M.data.paddle.width * 2;
        setTimeout(()=> {
            M.data.paddle.width =  M.data.paddle.width/2;
            M.resetBonus();
        }, 20000)
    },

    malus_SizeBar(){
        M.data.paddle.width =  M.data.paddle.width / 2;
        setTimeout(()=> {
            M.data.paddle.width =  M.data.paddle.width * 2;
            M.resetBonus();
        }, 20000)
    },

    bonus_addLifes(){
        M.data.ball.lifes = M.data.ball.lifes + 1;
    },

    malus_reverse_controls(){
        M.controls = -M.controls;
        setTimeout(()=> {
            M.controls = -M.controls;
            M.resetBonus();
        }, 20000);
    }


};
